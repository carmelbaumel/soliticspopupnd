var logger = require('../utils/popupLogger.js').logger.getLogger('metric');

var messageMap = require('../utils/messageMap.js');

var request = require('request');
var events = require('events').EventEmitter;
var emitter = new events.EventEmitter();

var apiUrl = "https://api.solitics.com/rest/promotions/registerNonCustomUserEventWithContentId/";

exports.createTask = function (metric) {
  emitter.emit('metric', metric);
}

emitter.on('metric', function(metric) {
  if (metric.messageId) {

    var popupMessage = messageMap.peek(metric.messageId);

    if (popupMessage) {
      if (popupMessage.webhookParams) {
        var pid = popupMessage.webhookParams.promotion_id;
        var cid = popupMessage.webhookParams.content_id;
        var sid = popupMessage.webhookParams.subscriberId;

        if (pid && cid && sid) {
          var opcode = metric.metric;

          var urlBuilder = apiUrl;
          urlBuilder += sid + '/';
          urlBuilder += pid + '/';
          urlBuilder += cid + '/';
          urlBuilder += 'POPUP/ESP_' + opcode.toUpperCase();

          logger.debug("Request:" + urlBuilder);

          request.post(
              urlBuilder,
              function (error, response) {
                if (!error && response.statusCode == 200) {
                  logger.debug('success');
                } else {
                  logger.error(error);
                }
              }
          );
        }
      }


    } else {
      logger.error('Received metric to an unknown popupMessage: ' + JSON.stringify(metric, null, 1));
    }

  } else {
    logger.error('Received metric without an ID: ' + JSON.stringify(metric, null, 1));
  }
});