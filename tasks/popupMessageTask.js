var logger = require('../utils/popupLogger.js').logger.getLogger('task');
var mkg = require('../utils/memberKeyGen.js');
var messageMap = require('../utils/messageMap.js');

var events = require('events').EventEmitter;
var emitter = new events.EventEmitter();

var tokens = require('../config/tokens.js');

var socketHandler = require('../handlers/socketHandler.js');

exports.createTask = function (popupMessage) {
  emitter.emit('popup', popupMessage);
}


emitter.on('popup', function(popupMessage) {
  if (tokens.auth(popupMessage.token)) {
    emitter.emit('authenticated', popupMessage);
  } else {
    logger.error('Token [' + popupMessage.token + '] not authenticated');
  }
});

emitter.on('authenticated', function(popupMessage) {
  var memberKey = mkg.gen(popupMessage.token, popupMessage.memberId);
  messageMap.add(popupMessage);

  var popup = {
    id: popupMessage.id,
    html: popupMessage.html,
    size: popupMessage.size,
    backdrop: popupMessage.backdrop,
    position: popupMessage.position,
    exitButton: popupMessage.exitButton,
    theme: popupMessage.theme,
    webhookParams: popupMessage.webhookParams
  };

  logger.debug(memberKey + ':: Handle message ' + JSON.stringify(popup)
  );
  socketHandler.sendPopup(memberKey, popup);
});

