/*
  Solitics Popup Provider v1.0
  08/17/18
  Boris Shogol
*/
var logger = require('./utils/popupLogger.js').logger.getLogger('default');

logger.info('=======================================================================================');
logger.info('Solitics POPUPS V1.0 Starting ' + new Date());
logger.info('=======================================================================================');

logger.debug('Running in ' + (process.env.NODE_ENV ? process.env.NODE_ENV : 'dev') + ' mode');

logger.debug('Starting SQS Listener');
require('./handlers/sqsHandler').start();

logger.debug('Starting SOCKET handler');
require('./handlers/socketHandler');

logger.debug('Starting App Metrics');
require('./appMetrics.js');