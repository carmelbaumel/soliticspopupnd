
var HashMap = require('hashmap');
var shortid = require('shortid');

var popupsMap = new HashMap();

exports.add = function (message) {

  var messageId = shortid.generate();
  message.id = messageId;

  popupsMap.set(messageId,  message);

  return message;
}

exports.peek = function (messageId) {

  var message = popupsMap.get(messageId);

  return message;

}