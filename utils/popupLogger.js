var log4js = require('log4js');
log4js.configure({
  appenders: {
    out: { type: 'stdout' },
    app: { type: 'dateFile', filename: './logs/application.log', pattern: '.yyyy-MM-dd-hh', compress: true }
  },
  categories: {
    default: { appenders: [ 'out', 'app' ], level: 'trace' },
    SQS: { appenders: [ 'out', 'app' ], level: 'trace' },
    SOCKET: { appenders: [ 'out', 'app' ], level: 'trace' },
    TASK: { appenders: [ 'out', 'app' ], level: 'trace' },
    METRIC: { appenders: [ 'out', 'app' ], level: 'trace' },
    APPMETRIC: { appenders: [ 'out', 'app' ], level: 'trace' }
  }
});
exports.logger = log4js;
