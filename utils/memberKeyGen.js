
var tokens = require('../config/tokens.js');

exports.gen = function (token, memberId) {
  return tokens.brand(token) + '_' + memberId;
}

exports.brand = function(token) {
  return tokens.brand(token);
}