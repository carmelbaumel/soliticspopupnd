var logger = require('./utils/popupLogger.js').logger.getLogger('appmetrics');

var HashMap = require('hashmap');
var tokens = require('./config/tokens.js');

var metricsMap = new HashMap();


var printMetrics = function () {

  logger.debug("======= App Metrics =======");
  metricsMap.forEach(function(metrics, brand) {
    logger.debug(JSON.stringify(metrics, null, 1));
  });
  logger.debug("======= App Metrics =======");
}; setInterval(printMetrics, 5000);

exports.addConnection = function (token) {

  var brand = tokens.brand(token);
  if (brand) {
    var brandMetrics = metricsMap.get(brand);

    if (!brandMetrics) {
      brandMetrics = {
        brand: brand,
        connections: 1,
        messages: 0
      }
    } else {
      brandMetrics.connections = brandMetrics.connections + 1;
    }

    metricsMap.set(brand, brandMetrics);
  }

}

exports.removeConnection = function (token) {

  var brand = tokens.brand(token);
  if (brand) {
    var brandMetrics = metricsMap.get(brand);
    if (brandMetrics) {
      brandMetrics.connections = brandMetrics.connections - 1;
      metricsMap.set(brand, brandMetrics);
    }
  }

}

exports.addMessage = function (token) {

  var brand = tokens.brand(token);
  if (brand) {
    var brandMetrics = metricsMap.get(brand);

    if (brandMetrics) {
      brandMetrics.messages = brandMetrics.messages + 1;
      metricsMap.set(brand, brandMetrics);
    }
  }

}
