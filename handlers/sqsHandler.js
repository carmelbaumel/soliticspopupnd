/**
 *
 * The popupsHandler is responsible for consuming message from AWS SQS.
 * Once a message is consumed it is sent to the event bus.
 * Once a message is processed it should be delete from the queue.
 *
 * @type {*}
 */
var logger = require('../utils/popupLogger.js').logger.getLogger('sqs');
var popupTasker = require('../tasks/popupMessageTask.js');

var AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var qurl;
if (process.env.NODE_ENV && process.env.NODE_ENV === 'production') {
  qurl = "https://sqs.eu-west-1.amazonaws.com/105449292765/pop-up-messages-bus";
} else {
  qurl = "https://sqs.eu-west-1.amazonaws.com/105449292765/test-pop-up-messages-bus";
}

var params = {
  AttributeNames: [
    "SentTimestamp"
  ],
  MaxNumberOfMessages: 1,
  MessageAttributeNames: [
    "All"
  ],
  QueueUrl: qurl,
  VisibilityTimeout: 20,
  WaitTimeSeconds: 20
};

exports.start = function() {
  logger.debug('Starting to listen for SQS popups');
  receiveMessage();
}

var receiveMessage = function() {
  logger.trace('Listening');

  sqs.receiveMessage(params, function(err, data) {
    if (err) {
      logger.error("Receive Error", err);
    }

    if (data.Messages) {
      if (data && data.Messages) {
        for (var i = 0; i < data.Messages.length; i++) {
          var message = data.Messages[i];
          try{
            var popupMessage = JSON.parse(message.Body);

            if (popupMessage.token && popupMessage.memberId) {
              popupTasker.createTask(popupMessage);
            } else {
              logger.error('Received Message but missing Token or MemberId' + JSON.stringify(message, null, 1));
            }

          } catch (e) {
            logger.error(e);
          } finally {
            deleteMessage(message);
            receiveMessage();
          }
        }
      }
    } else {
      setTimeout(function() {
        logger.trace('Waiting 15 seconds to start listening again');
        receiveMessage();
      }, 500);
    }
  });
}

var deleteMessage = function (message) {
  var deleteParams = {
    QueueUrl: qurl,
    ReceiptHandle: message.ReceiptHandle
  };
  sqs.deleteMessage(deleteParams, function(err, data) {
    if (err) {
      logger.error("Delete Error", err);
    } else {
      logger.trace("Message Deleted", data);
    }
  });
}