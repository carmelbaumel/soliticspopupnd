/**
 *
 * The socketHandler is responsible for:
 * 1. handling socket connections.
 * 2. sending message to the right connection.
 * 3. getting messages from the connection and sending as tasks.
 *
 *
 * @type {*}
 */
var logger = require('../utils/popupLogger.js').logger.getLogger('socket');
var mkg = require('../utils/memberKeyGen.js');
var pmt = require('../tasks/popupMetricTask.js');

var WebSocketServer = require('websocket').server;
var http = require('http');
var https = require('https');
var fs = require('fs');
var path = require('path');
var HashMap = require('hashmap');

var memberToConnection = new HashMap();
var brandConnectionCount = new HashMap();

var appMetrics = require('../appMetrics.js');

const NodeCache = require( "node-cache" );
const popupCache = new NodeCache({ stdTTL: 30, checkperiod: 30 });

popupCache.on( "expired", function(key){
  logger.debug('cache expired key[' + key + ']');
});

var server;
if (process.env.NODE_ENV && process.env.NODE_ENV === 'production') {
  var options = {
    key: fs.readFileSync(path.join(__dirname, '..', 'prv', 'private.pem')),
    cert: fs.readFileSync(path.join(__dirname, '..', 'prv', 'certificate.pem'))
  };
  server = https.createServer(options, function(request, response) {});
} else {
  server = http.createServer(function(request, response) {});
}
server.listen(8080, function() {});

// create the server
wsServer = new WebSocketServer({ httpServer: server });

// WebSocket server
// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function(request) {

    // accept connection - you should check 'request.origin' to
    // make sure that client is connecting from your website
  var connection = request.accept(null, request.origin);

  connection.on('message', function(message) {
    if (message.type == 'utf8') {
      try {
        handleRequest(JSON.parse(message.utf8Data), this);
      } catch (e) {
        logger.error(e);
      }
    } else {
      logger.error('Support only utf8 message: ' + message)
    }
  });

  connection.on('close', function(connection) {
    memberToConnection.delete(this.memberKey);
    appMetrics.removeConnection(this.token);
  });
});

var handleRequest = function(data, connection) {
  var requestType = data.requestType;

  if (requestType == 'connect') {
    handleRequestConnect(data, connection);
  }
  if (requestType == 'metric') {
    handleRequestMetric(data, connection);
  }
}

var handleRequestConnect = function(data, connection) {
  if (data.credentials) {
    var credentials = data.credentials;

    if (credentials.token && credentials.memberId) {
      var token = credentials.token;
      var memberId = credentials.memberId;

      var memberKey = mkg.gen(token, memberId);
      connection.token = token;
      connection.memberKey = memberKey;
      appMetrics.addConnection(token);
      memberToConnection.set(memberKey, connection);

      popupCache.get(memberKey, function(err, popupMessage){
        if (err) {
          logger.error(err);
        } else {
          if (popupMessage) {
            connection.send(JSON.stringify(popupMessage));
            //remove key from cache
            popupCache.del(memberKey);
          }
        }
      })
    }
  }
}

var handleRequestMetric = function(data, connection) {
  pmt.createTask(data);
}

exports.sendPopup = function (memberKey, popupMessage) {
  var connection = memberToConnection.get(memberKey);
  if (connection && connection.connected) {
    logger.debug(memberKey + ':: Sending popup: to ' + memberKey);
    connection.send(JSON.stringify(popupMessage));
    appMetrics.addMessage(connection.token);
      //remove key from cache
      //popupCache.del(memberKey);
  } else {
    logger.warn('Connection not found for member: ' + memberKey);
    memberToConnection.delete(memberKey);

    popupCache.set(memberKey, popupMessage, function(err) {
      if (err) {
        logger.error(err);
      } else {
        logger.trace('cache[' + memberKey + '] '
            + 'popup message[' + popupMessage.id + ']');
      }
    });
  }
}
