var HashMap = require('hashmap');
var appTokenMap = new HashMap();

appTokenMap.set('Wf9fsDARzdtCqDFJ9cVKrmuF', 'bmillion');
appTokenMap.set('iJyPFZHFhz99uXCZsQCoVduR', 'bmillion');

appTokenMap.set('KfduaVapnCEmq2wBv6qrLfyG', '24o');
appTokenMap.set('vPMohs3ndNQjfTesYtKT9NKp', '24cm');

appTokenMap.set('AGAZCNTHHts3FTtNWRKQugT4', 'avatrade');

appTokenMap.set('VqoNgfcjqg9igiW8oiqXHZdr', 'midwaygames');

appTokenMap.set('Zbg4eKgaaN8FHfaCLsckUYoQ', 'europrime');

appTokenMap.set('NL7cQ8fqXicxYqvCZtJAxHyU', 'brightertrade');
appTokenMap.set('MarDS3G2fStnEHYzEQ4H3xfk', 'fxvc');
appTokenMap.set('tfyTExynCQzttK2ema7fu4JP', 'gfc');
appTokenMap.set('d5rX7xvfQLzd7HPzQ3N5AtSG', 'tradeltd');
appTokenMap.set('swju8q574YkvuSr6CMXtSype', 'netopartners');
appTokenMap.set('fgsdpjmt94mnP4TYYbgPGCbG', 'q8trade');
appTokenMap.set('u7X2yqYwcjZ7PL3ajHZMbqJF', 'delasport');
appTokenMap.set('yN57XWXknD946nUYE6cRjA3W', 'winstar');
appTokenMap.set('WQNeJ*gbEMZ6t-BApXr!tWye', 'qteck');

appTokenMap.set('FfgyzNapjtgcjHVn8GHfYFG7', 'gmotrading');
appTokenMap.set('MBqUcNsRSHXagMQRmL84uXcp', 'etfinanceeu');
appTokenMap.set('cj7BcQwGHue8E9W9bExPtAzw', 'itrader');
appTokenMap.set('HZMgDa3dVjd73PK99GkWuenL', 'oinvest');

appTokenMap.set('6AkzoGLvbCDVLynWWkU2hN*!', 'netotech');
appTokenMap.set('Jk-shT3a78!z2rbjDdzyK.XY', 'nightrush');
appTokenMap.set('DvYdcQ8i9FNt.RvhW79ww6!V', 'maelys');
appTokenMap.set('nQRz5A5YbqucfHeS8hVRpzMk', 'avatrade');
appTokenMap.set('9nhmLUqGcxaZjtdN2fpf6LMj', 'wizpartner');
appTokenMap.set('YgbNbvSmYEt4B6uA3ZV6CyTW', 'itraderglobal');
appTokenMap.set('XvET3ghrSdddXbE5sTTmkg4t', 'fuse');

//for test only
//appTokenMap.set('Wf9fsDARzdtCqDFJ9cVKrmuF', 'Fashion');
exports.auth = function (token) {

  if (appTokenMap.get(token)) {
    return true;
  } else {
    return false;
  }

}

exports.brand = function (token) {
  return appTokenMap.get(token);
}